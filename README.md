# configuration

examples and stuff to experiment with ansible, salt and other configuration management systems

## ansible/my

- copy deploy-keys to ansible\my\management
- add fingerprints to .ssh/known_hosts via ssh-keygen <host> <host2> ...
- mgmt node reaches node02 

### todo

- transform openconnect-script to a ansible role:

openconnect-wrapper:

```bash
#!/bin/bash

PASSWD = `/bin/cat /usr/local/etc/openconnect/passwd` 

    /
    bin / echo $PASSWD | /usr/sbin / openconnect "$@"
```

vpnhsm.service:

```bash
[Unit]
Description = Connect to vpn.hs - merseburg.de(openconnect)
After = network.target

[Service]
Type = simple
ExecStart = /usr/local / bin / openconnect.wrapper vpn.hs - merseburg.de--user = hoenig--authgroup = 'SMK'--no - dtls--script = '/home/plumps/.venvs/vpn-splice/bin/vpn-slice web1.hsm=149.205.4.28'
Restart = always
User = root

[Install]
WantedBy = multi - user.target
```

#### node01 bastion host configuration for windows

```bash
ForwardAgent yes

Host web1.hsm
  User plumps
  IdentityFile c:\Users\plumps\.ssh\id_rsa
  ProxyCommand C:\Windows\System32\OpenSSH\ssh.exe bastion -W %h:%p

Host bastion
  HostName node01.publi.schtzngrmm.io
  Port 22022
  User plumps
  IdentityFile c:\Users\plumps\.ssh\id_rsa
```

#### node02 bastion host configuration for linux

```bash
ForwardAgent yes

Host web1.hsm
  User plumps
  IdentityFile ~/.ssh/node_deploy
  ProxyCommand ssh bastion -W %h:%p

Host bastion
  HostName node01.publi.schtzngrmm.io
  Port 22022
  User plumps
  IdentityFile ~/.ssh/node_deploy
```

#### TODOs for boxstarter 

- centralised credential management
- issues with WSL.ps1 (seems to work though…)

|Click link to run  |Description  |
|---------|---------|
|<a href='http://boxstarter.org/package/url?https://raw.githubusercontent.com/ua74e3dqka/configuration/master/boxstarter/ohmy.ps1'>ohmy</a>| work laptop|
|<a href='http://boxstarter.org/package/url?https://raw.githubusercontent.com/ua74e3dqka/configuration/master/boxstarter/test.1.ps1'>test</a>|boxstarter test call|
