{ lib, ... }: {
  # This file was populated at runtime with the networking
  # details gathered from the active system.
  networking = {
    nameservers = [
      "80.83.127.40"
      "8.8.4.4"
    ];
    defaultGateway = "130.255.77.1";
    defaultGateway6 = "2a02:e00:ffec::1";
    dhcpcd.enable = false;
    usePredictableInterfaceNames = lib.mkForce true;
    interfaces = {
      eth0 = {
        ipv4.addresses = [
          { address="130.255.77.61"; prefixLength=24; }
        ];
        ipv6.addresses = [
          { address="2a02:e00:ffec:4df::a"; prefixLength=64; }
{ address="2a02:e00:ffec:4df::9"; prefixLength=64; }
{ address="2a02:e00:ffec:4df::8"; prefixLength=64; }
{ address="2a02:e00:ffec:4df::7"; prefixLength=64; }
{ address="2a02:e00:ffec:4df::6"; prefixLength=64; }
{ address="2a02:e00:ffec:4df::5"; prefixLength=64; }
{ address="2a02:e00:ffec:4df::4"; prefixLength=64; }
{ address="2a02:e00:ffec:4df::3"; prefixLength=64; }
{ address="2a02:e00:ffec:4df::2"; prefixLength=64; }
{ address="2a02:e00:ffec:4df::1"; prefixLength=64; }
{ address="fe80::5062:8ff:feac:abe7"; prefixLength=64; }
        ];
        ipv4.routes = [ { address = "130.255.77.1"; prefixLength = 32; } ];
        ipv6.routes = [ { address = "2a02:e00:ffec::1"; prefixLength = 32; } ];
      };
      
    };
  };
  services.udev.extraRules = ''
    ATTR{address}=="52:62:08:ac:ab:e7", NAME="eth0"
    
  '';
}
