{ ... }: {
  imports = [
    ./hardware-configuration.nix
    ./networking.nix # generated at runtime by nixos-infect
    ./apps.nix
    ./nextcloud.nix
    
  ];

  boot.cleanTmpDir = true;
  networking.hostName = "web2.tru.io";
  networking.firewall.allowPing = true;
  services.openssh.enable = true;
  time.timeZone = "Europe/Berlin";
  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGCT9hImRSz4Qt8h6MuExgJhljb0Y01UZ0M992IdkHMF plumps@ohmy"
  ];


  # self-service
  system.autoUpgrade = {
    enable = true;
    allowReboot = true;
  };
 
  nix.gc.automatic = true;
  nix.gc.options = "-d";
}
