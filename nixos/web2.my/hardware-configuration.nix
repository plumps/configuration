{ ... }:
{
  imports = [ <nixpkgs/nixos/modules/profiles/qemu-guest.nix> ];
  boot.loader.grub.device = "/dev/vda";
  fileSystems."/" = { device = "/dev/vda1"; fsType = "ext4"; };
  fileSystems."/data" = { device = "/dev/vdb1"; fsType = "ext4"; };
  swapDevices = [ { device = "/dev/vda5"; } ];
}
