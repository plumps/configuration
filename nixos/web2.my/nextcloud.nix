{ pkgs, ... }:
{
  services.nginx.virtualHosts."web2.tru.io" = {
    addSSL = true;
    enableACME = true;
    # workaround - nextcloud not compatible with nginx acme #73585
    locations."^~ /.well-known/acme-challenge/" = {
	extraConfig = "allow all; root /var/lib/acme/acme-challenge; auth_basic off;"; 
    };
};
  
services.nextcloud = {
    enable = true;
    hostName = "web2.tru.io";
    nginx.enable = true;
    https = true;
    autoUpdateApps.enable = true;
    config = {
      dbtype = "pgsql";
      dbuser = "nextcloud";
      dbhost = "/run/postgresql"; # nextcloud will add /.s.PGSQL.5432 by itself
      dbname = "nextcloud";
      adminpassFile = "/data/nextcloud/web2.tru.io/adminpassFile";
      adminuser = "root";
    };
  };

  services.postgresql = {
    enable = true;
    ensureDatabases = [ "nextcloud" ];
    ensureUsers = [
     { name = "nextcloud";
       ensurePermissions."DATABASE nextcloud" = "ALL PRIVILEGES";
     }
    ];
  };

  # ensure that postgres is running *before* running the setup
  systemd.services."nextcloud-setup" = {
    requires = ["postgresql.service"];
    after = ["postgresql.service"];
  };

  networking.firewall.allowedTCPPorts = [ 80 443 ];
}
