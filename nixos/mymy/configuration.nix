# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ../common.nix
    ];

  # Kernel
  boot.kernelParams = [ "acpi_osi=!" "acpi_osi=\"Windows 2009\"" ];

  boot.initrd.luks.devices =  [
    {
      name = "root";
      device = "/dev/disk/by-uuid/9a8aaeef-9155-45a0-8d60-57c1d411aefd";
      preLVM = true;
      allowDiscards= true;
    }
  ];

  networking.hostName = "mymy"; # Define your hostname.

}
