# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./ohmy/hardware-configuration.nix
      ./common.nix
    ];

  # Kernel
  boot.kernelParams = [ "acpi_osi=!" "acpi_osi=\"Windows 2009\"" "acpi_backlight=vendor" ];
  # TODO: Linux > 4.19 isn't compatible with Nvidia < 418 (which is not part of 19.03)
  #boot.kernelPackages = pkgs.linuxPackages_latest;

  boot.initrd.luks.devices =  [
    {
      name = "crypt-root";
      device = "/dev/disk/by-uuid/83500f8f-b76f-4e3a-bc10-02da0e422f58"; 
      preLVM = true;
      allowDiscards= true;
    }
  ];

  networking.hostName = "ohmy"; # Define your hostname.

  #services.xserver.monitorSection = ''
  #  DisplaySize 508 285
  #'';
  services.xserver.dpi = 96;

}
