let 
  pkgs = import <nixpkgs> {};
in pkgs.runCommand "hello" { buildinputs = []; }
  ''
    echo hello > $out
  ''