let 
  pkgs = import <nixpkgs> {};
in pkgs.runCommand "hello" { buildinputs = []; }
  ''
	# pwd is not the local directory
    cp 1_hello.nix  $out
  ''