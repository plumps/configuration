let 
  pkgs = import <nixpkgs> {};
in pkgs.runCommand "hello" { buildinputs = []; }
  ''
	# assigns the path of 1_hello.nix to a variable
    cp ${./1_hello.nix}  $out
  ''