# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

let releaseVer = "19.03";

in

{
  imports = [
   <home-manager/nixos>
  ];

  nixpkgs.config.allowUnfree = true;

  nixpkgs.overlays = [ (self: super: {

      yed = super.callPackage ./pkgs/yed { };

      vscode-with-extensions = super.vscode-with-extensions.override {
        # When the extension is already available in the default extensions set.
        vscodeExtensions = with super.vscode-extensions; [
          bbenoist.Nix
          ms-vscode.cpptools
          ms-python.python
        ]
        # Concise version from the vscode market place when not available in the default set.
        ++ super.vscode-utils.extensionsFromVscodeMarketplace [
          {
            name = "code-runner";
            publisher = "formulahendry";
            version = "0.9.7";
            sha256 = "0brv0cblksv7nymdfnvmjx1wciagxcbyql9w5yas3b48j6hpq4ik";
          }

          {
            name = "language-haskell";
            publisher = "justusadam";
            version = "2.5.0";
            sha256 = "10jqj8qw5x6da9l8zhjbra3xcbrwb4cpwc3ygsy29mam5pd8g6b3";
          }

          {
            name = "LiveServer";
            publisher = "ritwickdey";
            version = "5.5.1";
            sha256 = "570a86fd4c9d7005e1d5ce5eb5abade8c8ea9dceeb5e937a937b8b8e633034f2";
          }
          
          {
            name = "Breeze-Dark-Theme";
            publisher = "AndrewFridley";
            version = "0.0.1";
            sha256 = "14bl32ln939rd75ip2yb2fwdwvp4pc0yk1hg8zck7h9vimzw5ky6";
          }

        ];
      };
    } ) 
  ];

  nix.allowedUsers = [ "plumps" ];
  # Weekly housekeeping
  nix.gc.automatic = true;
  nix.gc.dates = "weekly";
  nix.gc.options = "--delete-older-than 30d";

  # Kernel
  boot.extraModulePackages = with config.boot.kernelPackages; [ exfat-nofuse ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.extraHosts = ''
    130.255.76.32 web1.my
    130.255.77.61	web2.my
    149.205.4.28  web1.hsm
  '';

  networking.wireless.enable = false;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable = true;
  
  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 22 22000 ];
  networking.firewall.allowedUDPPorts = [ 21027 ];

  #networking.wireguard.interfaces.wg0 = {
  #  ips = [ "10.224.129.2/24" ];
  #  privateKeyFile = "/home/plumps/.wireguard/my";
  #  
  #  peers = [
  #      { allowedIPs = [ "0.0.0.0/0" ];
  #        endpoint = "web2.my:51820";
  #        publicKey = "e52Aq/3PYEKmjsDCGj5uLSarjXQlVqchJZgFNZKlOQk=";
  #        persistentKeepalive = 25;
  #      }
  #  ];
  #};


  fileSystems = 
  	let
  		makeCifsConfig = share:
  			{
  				device = share;
  				fsType = "cifs";
  				options = let
  				  # this line prevents hanging on network split
  				  automount_opts = "uid=plumps,gid=users,x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s";

  				in ["${automount_opts},credentials=/etc/nixos/secrets/smb"];
  			};
  	in {	
  			"/mnt/fileserver/media" = makeCifsConfig "//fileserver/media";
  			"/mnt/fileserver/data" = makeCifsConfig "//fileserver/data";
  	};

  # Select internationalisation properties.
  i18n = {
    consoleKeyMap = "neo";
    defaultLocale = "en_US.UTF-8";
  };

  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
     # utils
     curl gnupg p7zip unzip autojump htop xsel nmap 
     # system
     ark konsole okular gwenview ksysguard bluedevil veracrypt spectacle yakuake wireguard
     # language
     hunspellDicts.de-de
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs.adb.enable = true;
  programs.mtr.enable = true;
  programs.ssh.startAgent = false;
  programs.gnupg.agent = { enable = true; enableSSHSupport = true; };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  
  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;
  hardware.pulseaudio.package = pkgs.pulseaudioFull;
  hardware.bluetooth.extraConfig = ''
    [general]
    Enable=Source,Sink,Media,Socket
  '';

  # Enable bluetooth
  hardware.bluetooth.enable = true;
  hardware.bluetooth.powerOnBoot = false;

  services.printing.enable = true;
  services.printing.drivers = [ pkgs.gutenprint pkgs.hplip ];
  # Enable syncthing for plumps
  services.syncthing = {
     enable = true;
     user = "plumps";
     group = "users";
     configDir = "/home/plumps/.syncthing";
  };

  services.redshift = {
     enable = true;
     latitude = "51.5";
     longitude = "12";
  };

  services.tor = {
    enable = true;
    client = {
      enable = true;
      privoxy.enable = true;
    };
  };

  # Enable the X11 windowing system.
  services.xserver = {
    enable = true;
    layout = "de";
    xkbVariant = "neo";
    videoDrivers = [ "nvidia" ];

    displayManager.sddm.enable = true;
    desktopManager.plasma5 = {
      enable = true;
      enableQt4Support = false;
    };
    
    libinput.enable = true;
  };
  
  # graphics
  # (for steam)

  hardware.nvidia = {
	modesetting.enable = true;
	optimus_prime.enable = true;
        optimus_prime.intelBusId = "PCI:0:2:0";
        optimus_prime.nvidiaBusId = "PCI:1:0:0";
  };

  hardware.opengl.driSupport32Bit = true;
  hardware.pulseaudio.support32Bit = true;

  powerManagement.enable = true;
  powerManagement.cpuFreqGovernor = "ondemand";

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.plumps = {
    createHome = true;
    extraGroups = [ "cdrom" "wheel" "video" "audio" "disk" "networkmanager" "vboxusers" "docker" ];
    group = "users";
    isNormalUser = true;
    uid = 1000;
  };
  
  home-manager.useUserPackages = true;
  home-manager.users.plumps = {
    nixpkgs.config = import /home/plumps/.config/nixpkgs/config.nix;
    home.packages = with pkgs; [
      # utils 
      httpie home-manager jq ripgrep
      # 3d/2d/video
      blender gimp
      # apps
      firefox thunderbird gajim tdesktop keepassxc chromium qbittorrent quaternion
      # games
      scid-vs-pc
      # dev
      gnumake vagrant docker-compose yed (callPackage ./pkgs/astah-community {})
      # multimedia
      mpv youtube-dl vlc kdenlive frei0r ffmpeg-full cmus handbrake steam gpodder
      # office
      libreoffice-fresh calibre
      # font
      iosevka fira-code
      
    ];

    programs.home-manager = {
      enable = true;
    };

    programs.bash = {
      enable = true;
      enableAutojump = true;

      initExtra = ''
        dtags () {     local image="$1";      wget -q https://registry.hub.docker.com/v1/repositories/"$image"/tags -O -         | sed -e 's/[][]//g' -e 's/"//g' -e 's/ //g'         | tr '}' '\n'  | awk -F: '{print $3}'; }
      '';

      shellAliases = { 
        
        "mount_defaults" = "veracrypt --fs-options=iocharset=utf8,users,uid=$(id -u),gid=$(id -g),fmask=0113,dmask=0002 --mount /home/plumps/Container/defaults.vc /mnt/container/defaults"; 
        
    };
    };

    programs.git = {
      enable = true;
      userName = "plumps";
      userEmail = "plumps@derkohl.nohost.me";
    };

    programs.neovim = {
      enable = true;
      vimAlias = true;
      configure = {
        customRC = ''
          let mapleader = ","
	  :map <leader>p :!nixos-rebuild switch<CR>
        '';
      };
    };

    programs.ssh = {
      enable = true;
      forwardAgent = true;
      matchBlocks = {
        "*.my" = {
          user = "plumps";
        };

        "*.hsm" = {
          user = "plumps";
          extraOptions = {
            ProxyCommand = "ssh bastion -W %h:%p";
          };
        };

        bastion = {
          user = "plumps";
          hostname = "web1.my";
          port = 22022;
        };

        github = {
          hostname = "github.com";
          user = "git";
        };

      };
    };


    programs.vscode = {
      enable = true;
      userSettings = {
	      "editor.fontSize" = 14;
        "dayNightThemeSwitcher.dayTheme" = "Atom One Light";
        "dayNightThemeSwitcher.nightTheme" = "Atom One Dark";
        "editor.acceptSuggestionOnEnter" = "off";
        "editor.cursorBlinking" = "smooth";
        "editor.cursorSmoothCaretAnimation" = true;
        "editor.fontFamily" = "Hack, 'Fira Code', Consolas, 'Courier New', monospace";
        "editor.fontLigatures" = true;
        "editor.fontWeight" = "normal";
        "editor.formatOnPaste" = true;
        "editor.formatOnSave" = true;
        "editor.formatOnType" = true;
        "editor.insertSpaces" = true;
        "editor.tabCompletion" = "on";
        "editor.wordWrap" = "wordWrapColumn";
        "explorer.confirmDelete" = false;
        "explorer.confirmDragAndDrop" = false;
        "explorer.sortOrder" = "type";
        "extensions.autoCheckUpdates" = false;
        "extensions.autoUpdate" = false;
        "extensions.ignoreRecommendations" = true;
        "extensions.showRecommendationsOnlyOnDemand" = true;
        "files.autoSave" = true;
        "files.trimFinalNewlines" = true;
        "git.autofetch" = false;
        "material-icon-theme.showWelcomeMessage" = false;
        "python.linting.enabled" = true;
        "python.linting.pylintEnabled" = true;
        "sync.autoUpload" = true;
        "sync.gist" = "9070d9540caa3af058fb3b41040ea9da";
        "telemetry.enableCrashReporter" = false;
        "telemetry.enableTelemetry" = false;
        "terminal.integrated.shell.windows" = "";
        "update.enableWindowsBackgroundUpdates" = false;
        "update.mode" = "none";
        "update.showReleaseNotes" = false;
        "window.menuBarVisibility" = "toggle";
        "workbench.colorCustomizations" = {};
        "workbench.colorTheme" = "Atom One Light";
        "workbench.editor.highlightModifiedTabs" = true;
        "workbench.list.keyboardNavigation" = "filter";
        "workbench.sideBar.location" = "right";
        "workbench.startupEditor" = "none";
      };
    };
    xdg.configFile."mpv/mpv.conf".source = ./dotfiles/mpv.conf; 
    xdg.configFile."mpv/script-opts/osc.conf".source = ./dotfiles/mpv_osc.conf; 

    services.mpd = {
        enable = true;
	  };
  };

  programs.bash = {
	enableCompletion = true;
  };

  virtualisation =  {
    docker.enable = true;
  	virtualbox.host = {
  		enable = true;
  		enableExtensionPack = true;
  	};
  };
  
  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = releaseVer ; # Did you read the comment?

}
