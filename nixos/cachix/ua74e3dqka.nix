
{
  nix = {
    binaryCaches = [
      "https://ua74e3dqka.cachix.org"
    ];
    binaryCachePublicKeys = [
      "ua74e3dqka.cachix.org-1:JDfCbosJdKxfDOWgKcTzSmKAqnInFxjB7KlM+O1trlM="
    ];
    trustedUsers = [ "root" "plumps" ];
  };
}
    