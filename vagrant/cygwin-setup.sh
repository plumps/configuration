# sets .vagrant folder to $HOME/.vagrant_t5 (format of .vagrant is not portable per se)
export VAGRANT_DOTFILE_PATH=/cygdrive/c/Users/plumps/.vagrant
# forces the use of native ssh tools
export VAGRANT_PREFER_SYSTEM_BIN=1
#
export VAGRANT_HOME=/cygdrive/c/Users/plumps/

env | grep VAGRANT