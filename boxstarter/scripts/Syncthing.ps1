# syncthing
# get path to latest syncthing install
If (-Not (Get-Service Syncthing -ErrorAction SilentlyContinue)) {
	$cred = Get-Credential
	choco install syncthing
	$syncthingpath = Get-ChildItem -Directory C:\ProgramData\chocolatey\lib\syncthing\tools | Sort-Object CreationTime -Descending | Select-Object -First 1 | Select-Object -expand fullname
	nssm install syncthing $syncthingpath
	nssm set syncthing AppParameters "-no-console -no-browser -home=\"C:\Users\plumps\AppData\Local\Syncthing\\""
	nssm set syncthing AppExit Default Exit
	nssm set syncthing AppExit 4 Restart
	nssm set syncthing AppExit 0 Exit
	nssm set syncthing AppExit 3 Restart
	nssm set syncthing ObjectName .\$cred.UserName $cred.GetNetworkCredential().password
	nssm set syncthing start SERVICE_DELAYED_AUTO_START
	netsh advfirewall firewall add rule name="Syncthing Port 8384" dir=in action=allow protocol=TCP localport=8384
	Start-Service Syncthing
}