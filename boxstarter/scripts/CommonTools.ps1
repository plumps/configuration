# tools we expect across all
choco install 7zip.install

choco install git --package-parameters="'/GitAndUnixToolsOnPath /WindowsTerminal'"
[System.Environment]::SetEnvironmentVariable('GIT_SSH', 'C:\Windows\System32\OpenSSH\ssh.exe', [System.EnvironmentVariableTarget]::User)

choco install nssm
choco install powershell-core
choco install sysinternals
choco install veracrypt
choco install windirstat