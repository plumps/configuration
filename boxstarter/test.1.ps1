Disable-UAC

Write-Host '>>>'
$helperUri = $Boxstarter['ScriptToCall']
$bstrappackage = '-bootstrapPackage'
$strpos = $helperUri.IndexOf($bstrappackage)
# 
$helperUri = $helperUri.Substring($strpos + $bstrappackage.Length)
$helperUri = $helperUri.TrimStart("'", " ")
$helperUri = $helperUri.TrimEnd("'", " ")
$helperUri = $helperUri.Substring(0, $helperUri.LastIndexOf("/"))
$helperUri += "/scripts"

function executeScript {
	Param ([string]$script)
	write-host "executing $helperUri/$script ..."
	#(New-Object net.webclient).DownloadString("$helperUri/"
	Invoke-Expression ((new-object net.webclient).DownloadString("$helperUri/$script"))
}

executeScript "TestFunction.ps1"

Write-Host '<<<'

Enable-UAC