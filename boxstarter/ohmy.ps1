# Description: Boxstarter Script for dev machine
# Original Author: Microsoft, Jess Frazelle <jess@linux.com>
# Author: plumps

# Install boxstarter:
# 	. { iwr -useb http://boxstarter.org/bootstrapper.ps1 } | iex; get-boxstarter -Force
#
# You might need to set: Set-ExecutionPolicy RemoteSigned

# Run this boxstarter by calling the following from an **elevated** command-prompt:
# 	start http://boxstarter.org/package/nr/url?<URL-TO-RAW-GIST>
# OR
# 	Install-BoxstarterPackage -DisableReboots -PackageName <URL-TO-RAW-GIST> 
#
# Learn more: http://boxstarter.org/Learn/WebLauncher

Disable-UAC

# Get the base URI path from the ScriptToCall value
$helperUri = $Boxstarter['ScriptToCall']
$bstrappackage = '-bootstrapPackage'
$strpos = $helperUri.IndexOf($bstrappackage)
# 
$helperUri = $helperUri.Substring($strpos + $bstrappackage.Length)
$helperUri = $helperUri.TrimStart("'", " ")
$helperUri = $helperUri.TrimEnd("'", " ")
$helperUri = $helperUri.Substring(0, $helperUri.LastIndexOf("/"))
$helperUri += "/scripts"

function executeScript {
	Param ([string]$script)
	write-host "executing $helperUri/$script ..."
	#(New-Object net.webclient).DownloadString("$helperUri/"
	Invoke-Expression ((new-object net.webclient).DownloadString("$helperUri/$script"))
}

# --- Setting up Windows ---
executeScript "Syncthing.ps1";

executeScript "Browsers.ps1";
executeScript "CommonTools.ps1";
executeScript "DevTools.ps1"
executeScript "FileExplorerSettings.ps1";
executeScript "RemoveDefaultApps.ps1";
executeScript "SystemConfiguration.ps1";
executeScript "Virtualbox.ps1";


executeScript "WSL.ps1";
RefreshEnv


#--- Apps ---
choco install audacity
choco install gimp
choco install ghostscript
choco install keepassxc
choco install mpv
choco install qbittorrent
choco install sublimetext3

#--- Fonts ---
choco install firacode


# Install tools in WSL instance
write-host "Installing tools inside the WSL distro..."
Ubuntu1804 run apt-get install ansible -y

# Checkout projects
mkdir $env:HOMEPATH\project\
Set-Location $env:HOMEPATH\project
git.exe clone https://github.com/ua74e3dqka/configuration.git
git.exe clone https://github.com/ua74e3dqka/p5js.git
git.exe clone https://github.com/ua74e3dqka/sublime_settings.git
git.exe clone https://github.com/ua74e3dqka/powershell.git
git.exe clone https://github.com/ua74e3dqka/container.git
git.exe clone https://github.com/ua74e3dqka/schreibe-dein-programm.git
git.exe clone https://github.com/ua74e3dqka/The-Nature-Of-Code-Kadenze.git

# set desktop wallpaper
Set-ItemProperty -Path 'HKCU:\Control Panel\Desktop' -Name wallpaper -Value "$env:HOMEDRIVE$env:HOMEPATH\project\configuration\boxstarter\data\wallpaper.png"
rundll32.exe user32.dll, UpdatePerUserSystemParameters
RefreshEnv

$computername = "ohmy"
if ($env:computername -ne $computername) {
	Rename-Computer -NewName $computername
}

Enable-UAC
Enable-MicrosoftUpdate
Install-WindowsUpdate -acceptEula